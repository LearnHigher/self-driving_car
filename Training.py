import tensorflow as tf
import numpy as np
import pandas as pd

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Dense, Dropout

data = pd.read_csv('data.csv', skiprows=[0], header=None)

print(data.describe)
# 0 là left, 1 là right

print(data.head())

data = data.astype('float32')

print(data.head())

data = data.to_numpy()

print(data.shape)

training = data[0:66376]
testing = data[66376:]

training_features = training[:, 0:-1]
training_labels = training[:, -1]
testing_features = testing[:, 0:-1]
testing_labels = testing[:, -1]

print(training_features)
print(testing_labels)

training_features[0].shape

model = Sequential()
model.add(Input(shape=(4,)))
model.add(Dense(16, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(8, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1, activation='sigmoid'))

model.compile(optimizer='sgd', loss='binary_crossentropy', metrics=['accuracy'])

model.fit(training_features, training_labels, epochs=100)

model.save('lane.h5')

test_loss, test_accuracy = model.evaluate(testing_features, testing_labels)

predicts = model.predict(testing_features)

print(predicts)