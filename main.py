import cv2
import numpy as np
import matplotlib.pyplot as plt
import math


def canny(img):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    kernel = 5
    blur = cv2.GaussianBlur(gray, (kernel, kernel), 0)
    canny = cv2.Canny(blur, 50, 250)
    return canny


def region_of_interest(img):
    height = img.shape[0]
    width = img.shape[1]
    mask = np.zeros_like(img)
    polygon = np.array([[(250, height - 80), (535, 460), (800, 460), (1080, height - 80)]], np.int32)
    cv2.fillPoly(mask, polygon, (255, 255, 255))
    mask_image = cv2.bitwise_and(img, mask)
    return mask_image


def houghLines(img):
    houghLines = cv2.HoughLinesP(img, 2, np.pi / 180, 100, np.array([]), minLineLength=40, maxLineGap=5)
    return houghLines


def display_lines(img, lines):
    line_image = np.zeros_like(img)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                # cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
                cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 10)
    return img


def make_point(img, lineSI):
    slope, intercept = lineSI
    height = img.shape[0]
    y1 = int(height)
    y2 = int(y1 * 3 / 5)
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return [[x1, y1, x2, y2]]


def findMidLane(line1, line2):
    result = ((np.array(line1) + np.array(line2)) / 2).round()
    print('Middle lane: ', result)
    return result


def drawMidLane(img, midLane):
    x1 = 643
    y1 = 720
    x2 = int(midLane[0][2])
    y2 = int(midLane[0][3])

    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 10)

    return img


def average_slope_intercept(img, lines):
    left_fit = []
    right_fit = []
    for line in lines:
        for x1, y1, x2, y2 in line:
            fit = np.polyfit((x1, x2), (y1, y2), 1)
            slope = fit[0]
            intercept = fit[1]
            if slope < 0:
                left_fit.append((slope, intercept))
            else:
                right_fit.append((slope, intercept))
    left_fit_average = np.average(left_fit, axis=0)
    right_fit_average = np.average(right_fit, axis=0)
    left_line = make_point(img, left_fit_average)
    right_line = make_point(img, right_fit_average)
    average_lines = [left_line, right_line]
    return average_lines


def calculateDistance(pt1, pt2):
    return math.sqrt(((pt1[0] - pt2[0]) ** 2) + ((pt1[1] - pt2[1]) ** 2))


def calculateAngle(pt1, pt2, pt3):
    d1 = calculateDistance(pt1, pt3)
    d2 = calculateDistance(pt2, pt3)

    tanAngle = d1 / d2

    angleR = math.atan(tanAngle)
    angleD = math.degrees(angleR)

    return angleD


capture = cv2.VideoCapture('test5.mp4')

while capture.isOpened():
    try:
        ret, image = capture.read()
        frame = np.copy(image)
        if ret:

            scr = np.float32([(535, 460),
                              (150, 640),
                              (1000, 640),
                              (800, 460)])

            dst = np.float32([(100, 0),
                              (100, 720),
                              (900, 720),
                              (900, 0)])

            M = cv2.getPerspectiveTransform(scr, dst)
            M_inv = cv2.getPerspectiveTransform(dst, scr)


            def front_to_top(img):
                size = (1280, 720)
                return cv2.warpPerspective(img, M, size, flags=cv2.INTER_LINEAR)


            def top_to_front(img):
                size = (1280, 720)
                return cv2.warpPerspective(img, M_inv, size, flags=cv2.INTER_LINEAR)


            # canny edge detector
            canny_output = canny(frame)

            # region of interest
            mask_output = region_of_interest(canny_output)

            # inverse Perspective Mapping
            # output_top = top_to_front(mask_output)
            # output_front = front_to_top(mask_output)

            # probabilistic hough transform
            lines = houghLines(mask_output)
            average_lines = average_slope_intercept(mask_output, lines)
            print(average_lines)
            line_image = display_lines(frame, average_lines)

            # find center lane
            midline_result = findMidLane(average_lines[0], average_lines[1])
            centerLine = drawMidLane(image, midline_result)

            # calculate angle
            angle = calculateAngle((midline_result[0][2], midline_result[0][3]), (643, 720), (643, 432))
            print('Angle: ', angle)

            # show the results
            # cv2.imshow('normal', frame)
            # cv2.imshow('region of interest', mask_output)
            # cv2.imshow('inverse Perspective Mapping', output_front)
            # cv2.imshow('canny edge detector', canny_output)
            cv2.imshow('probabilistic hough transform', centerLine)
            # plt.imshow(frame)
            # plt.show()

            if cv2.waitKey(25) & 0xff == ord('q'):
                break
        else:
            break
    except Exception:
        pass
capture.release()
cv2.destroyAllWindows()
